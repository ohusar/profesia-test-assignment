# Ondrej Husár - Profesia Test Assignment

- [Setup](#setup)
- [Known "issues"](#known-issues)

## Setup

Clone the repo.

```git
git clone git@gitlab.com:ohusar/profesia-test-assignment.git
```

Install backend dependencies.

```bash
cd profesia-test-assignment/be
npm i
npm run start
```

The babel transpilation to dist/ takes few seconds.

```bash
$ npm run start
...

Error: Cannot find module '...profesia-test-assignment/be/dist'

[nodemon] app crashed - waiting for file changes before starting...
```

But after successfull compilation the server should restart.

```bash

Successfully compiled ... files with Babel (331ms).
[nodemon] restarting due to changes...

Running at http://localhost:8081/graphql
```

Keep the server running and fire up the frontend server.

```bash
cd profesia-test-assignment/fe
npm i
npm start
```

Go to [http://localhost:3000](http://localhost:3000) and you should be up and running.

![screen](./screenshot1.png)
![screen](./screenshot2.png)

## Running other things

### FE

#### cypress tests

```
npm run cypress:open
```

### BE

#### database (db.sqlite is commited)

##### create-schema

Creates db tables.

```
npm run create-schema
```

##### create-schema

Wipes db & populates it with example data.

```
npm run popualte-db
```

## Known issues

### FE

- The Items drag & drop is not connected to BE - it's not persistent
